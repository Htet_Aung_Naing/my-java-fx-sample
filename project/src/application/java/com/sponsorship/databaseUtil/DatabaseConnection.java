package com.sponsorship.databaseUtil;

import java.sql.Connection;
import java.sql.DriverManager;



public class DatabaseConnection {
	
	public static Connection getConnection() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionurl = "jdbc:sqlserver://scimmrasist:1433;" +  
	         "databaseName=47;user=ASIST_RPT;password=$Asist_rpt"; 
			Connection con = null;		
			con = DriverManager.getConnection(connectionurl);
	
			return con;
		} catch (Exception ex) {
			System.out.println("Database.getConnection() Error -->"
					+ ex.getMessage());
			return null;
		}
	}

	public static void close(Connection con) {
		try {
			con.close();
		
		} catch (Exception ex) {
		}
	}
	
	

}

