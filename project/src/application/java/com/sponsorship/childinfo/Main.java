package com.sponsorship.childinfo;
	
import java.sql.SQLException;
import java.util.ArrayList;

import com.sponsorship.dao.ChildDao;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Sponsorship");
		Scene scene = new Scene(new VBox(), 400, 350);
        scene.setFill(Color.OLDLACE);
 
        MenuBar menuBar = new MenuBar();
 
        // --- Menu File
        Menu menuFile = new Menu("File");
 
        // --- Menu Edit
        Menu menuEdit = new Menu("Edit");
 
        // --- Menu View
        Menu menuView = new Menu("View");
 
        menuBar.getMenus().addAll(menuFile, menuEdit, menuView);
 
        MenuItem add = new MenuItem("Shuffle");
            add.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent t) {
                    
                }
            });
            menuFile.getItems().add(add);
        ((VBox) scene.getRoot()).getChildren().addAll(menuBar);
 
        primaryStage.setScene(scene);
        primaryStage.show();
	}
	
	public static void main(String[] args) throws SQLException {
		//launch(args);
		
		ArrayList<Children> childrenList = new ArrayList<Children>();
		ChildDao childDao = new ChildDao();
		childrenList = childDao.getUpstreamChildrenList();
		int i = 1;
		for (Children children : childrenList) {
			System.out.println(i+". "+"Child ID : "+children.getChId()+";"+" Child Name : "+children.getChName());
			System.out.println("Gender : "+children.getGender()+";"+" DOB : "+children.getDob());
			System.out.println("School Name : "+children.getSchoolName());
			for ( ChildAttributeData attr : children.getAttributeList()) {
				System.out.println(attr.attributeCode +" : "+attr.propCode);
			}
			System.out.println(".................................");
			i++;
		}
	}
}
