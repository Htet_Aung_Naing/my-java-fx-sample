package com.sponsorship.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.sponsorship.childinfo.ChildAttributeData;
import com.sponsorship.childinfo.Children;
import com.sponsorship.databaseUtil.DatabaseConnection;

public class ChildDao {
	
	public ArrayList<Children> getUpstreamChildrenList() throws SQLException
	{
		ArrayList<Children> res = new ArrayList<Children>();
		Connection con = DatabaseConnection.getConnection();
		String query = "Select * from CHILD where last_sid = 59023";
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			Children ch = new Children();
			ch.setChId(rs.getString("child_id"));
			ch.setPerid(rs.getInt("per_id"));
			ch.setSchoolid(rs.getInt("school_id"));
			getPersonData(ch, con);
			res.add(ch);
		 }
		 con.close();
		return res;
	}
	
	public Children getAttributePropertyCodeData(Children ch , Connection con) throws SQLException
	{
		Statement st = con.createStatement();
		String query = "Select * from CH_PROFILE where per_id = "+ ch.getPerid();
		ResultSet rs = st.executeQuery(query);
		ArrayList<ChildAttributeData> attrList = new ArrayList<ChildAttributeData>();
		while(rs.next())
		{
			ChildAttributeData attribute = new ChildAttributeData();
			String attrQuery = "select * from CH_ATTR_CODE where ch_attr_code ="+ rs.getInt("ch_attr_code");
			st = con.createStatement();
			ResultSet rsattr = st.executeQuery(attrQuery);
			if(rsattr.next())
				attribute.setAttributeCode(rsattr.getString("ch_attr_descr"));
			String propQuery = "select * from CH_PROP_CODE where ch_attr_code="+rs.getInt("ch_attr_code")+" and ch_prop_code="
					+ rs.getInt("ch_prop_code");
			st = con.createStatement();
			ResultSet rsProp = st.executeQuery(propQuery);
			if(rsProp.next())
			{
				attribute.setPropCode(rsProp.getString("ch_prop_descr"));
			}
			attrList.add(attribute);
		}
		ch.setAttributeList(attrList);
		return ch;
	}
	
	public Children getPersonData(Children ch,Connection con) throws SQLException
	{
		String photoquery = "Select photo_path from CH_PHOTO where per_id = "+ch.getPerid()+" and is_active = 'Y' and is_main = 'Y'";
		String query = "Select * from PERSON where per_id = "+ch.getPerid();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		String schoolquery = "Select * from School where school_id = "+ch.getSchoolid();
		st = con.createStatement();
		ResultSet rsSchool = st.executeQuery(schoolquery);
		if(rsSchool.next())
		{
			ch.setSchoolName(rsSchool.getString("school_name"));
		}
		
		 while(rs.next())
		 {
			 ch.setChName(rs.getString("per_full"));
			 ch.setDob(new SimpleDateFormat("MM-dd-yyyy").format(rs.getDate("date_of_birth")));
			 ch.setGender(rs.getString("sex_code"));
		 }
		  
		 ch = getAttributePropertyCodeData(ch, con);
		 
		 ResultSet rsphoto = st.executeQuery(photoquery);
		 
		 if(rsphoto.next())
		 {
			 ch.setPhotopath(rsphoto.getString("photo_path"));
		 }
	
		return ch;
	}

}
