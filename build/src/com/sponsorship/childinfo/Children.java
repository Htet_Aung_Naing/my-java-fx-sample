package com.sponsorship.childinfo;

import java.util.ArrayList;

public class Children {
	
	String chName;
	String chId;
	String dob;
	String gender;
	String schoolName;
	String photopath;
	int perid;
	int schoolid;
	ArrayList<ChildAttributeData> attributeList;
	
	public ArrayList<ChildAttributeData> getAttributeList() {
		return attributeList;
	}
	public void setAttributeList(ArrayList<ChildAttributeData> attributeList) {
		this.attributeList = attributeList;
	}
	public Integer getPerid() {
		return perid;
	}
	public void setPerid(int perid) {
		this.perid = perid;
	}

	public String getChName() {
		return chName;
	}
	public void setChName(String chName) {
		this.chName = chName;
	}
	public String getChId() {
		return chId;
	}
	public void setChId(String chId) {
		this.chId = chId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	
	public int getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(int schoolid) {
		this.schoolid = schoolid;
	}
	public String getPhotopath() {
		return photopath;
	}
	public void setPhotopath(String photopath) {
		this.photopath = photopath;
	}
	public Children()
	{
		this.chId = "";
		this.chName = "";
		this.attributeList = new ArrayList<ChildAttributeData>();
		this.schoolName = "";
		this.dob = "";
		this.gender = "";
		this.perid = 0;
		this.schoolid = 0;
		this.photopath = "";
	}
	

}
