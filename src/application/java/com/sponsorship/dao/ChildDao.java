package com.sponsorship.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.sponsorship.childinfo.ChildAttributeData;
import com.sponsorship.childinfo.Children;
import com.sponsorship.childinfo.Person;
import com.sponsorship.databaseUtil.DatabaseConnection;

public class ChildDao {
	
	public ArrayList<Children> getUpstreamChildrenList() throws SQLException
	{
		ArrayList<Children> res = new ArrayList<Children>();
		Connection con = DatabaseConnection.getConnection();
		String query = "Select * from CHILD where last_sid = 0";
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 int i = 1;
		 while(rs.next())
		 {
			Children ch = new Children();
			ch.setChId(i+". "+rs.getString("child_id"));
			ch.setPerid(rs.getInt("per_id"));
			ch.setSchoolid(rs.getInt("school_id"));
			getPersonData(ch, con);
			res.add(ch);
			i++;
		 }
		 con.close();
		return res;
	}
	
	public ArrayList<Children> getUpstreamChildrenListbyIdlist(String cidList) throws SQLException
	{
		ArrayList<Children> res = new ArrayList<Children>();
		Connection con = DatabaseConnection.getConnection();
		String query = "Select * from CHILD where child_id in ("+cidList+")";
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 int i = 1;
		 while(rs.next())
		 {
			Children ch = new Children();
			ch.setChId(i+". "+rs.getString("child_id"));
			ch.setPerid(rs.getInt("per_id"));
			ch.setSchoolid(rs.getInt("school_id"));
			getPersonData(ch, con);
			res.add(ch);
			i++;
		 }
		 con.close();
		return res;
	}
	
	public ArrayList<Person> getIneligibleChildrenList() throws SQLException
	{
		ArrayList<Person> res = new ArrayList<Person>();
		Connection con = DatabaseConnection.getConnection();
		String query = "Select * from Person where per_id in (select distinct(per_id) from CH_STAT_HIST where ch_stat_code<>001)";
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);

		 while(rs.next())
		 {
			Person per = new Person();
			per.setPerid(rs.getInt("per_id"));
			per.setChild_name(rs.getString("per_full"));
			per.setIneligible_date(new SimpleDateFormat("MM-dd-yyyy").format(rs.getDate("add_date")));
			String querychild = "Select * from CHILD where per_id="+rs.getInt("per_id");
			st = con.createStatement();
			ResultSet rsattr = st.executeQuery(querychild);
			if(rsattr.next())
			{
				per.setChild_id(String.valueOf(rsattr.getInt("child_id")));
				per.setSchool_id(rsattr.getInt("school_id"));
				String queryschool = "Select * from School where school_id="+per.getSchool_id();
				st = con.createStatement();
				ResultSet rsschool = st.executeQuery(queryschool);
				if(rsschool.next())
					per.setSchoolname(rsschool.getString("school_name"));
			}
				
			res.add(per);
		 }
		 con.close();
		return res;
	}
	
	public ArrayList<Person> getIneligiblePersonList()
	{
		ArrayList<Person> resList = new ArrayList<>();
		
		
		
		return resList;
	}
	
	public Children getAttributePropertyCodeData(Children ch , Connection con) throws SQLException
	{
		Statement st = con.createStatement();
		String query = "Select * from CH_PROFILE where per_id = "+ ch.getPerid();
		ResultSet rs = st.executeQuery(query);
		ArrayList<ChildAttributeData> attrList = new ArrayList<ChildAttributeData>();
		ArrayList<ChildAttributeData> schoolattList = new ArrayList<>();
		while(rs.next())
		{
			ChildAttributeData attribute = new ChildAttributeData();
			String attrQuery = "select * from CH_ATTR_CODE where ch_attr_code ="+ rs.getInt("ch_attr_code");
			st = con.createStatement();
			ResultSet rsattr = st.executeQuery(attrQuery);
			if(rsattr.next())
				attribute.setAttributeCode(rsattr.getString("ch_attr_descr"));
			String propQuery = "select * from CH_PROP_CODE where ch_attr_code="+rs.getInt("ch_attr_code")+" and ch_prop_code="
					+ rs.getInt("ch_prop_code");
			st = con.createStatement();
			ResultSet rsProp = st.executeQuery(propQuery);
			if(rsProp.next())
			{
				attribute.setPropCode(rsProp.getString("ch_prop_descr"));
			}
			
			if(attribute.getAttributeCode().equalsIgnoreCase("School Name") || attribute.getAttributeCode().equalsIgnoreCase("School attendance")
					|| attribute.getAttributeCode().equalsIgnoreCase("Type of school") || attribute.getAttributeCode().equalsIgnoreCase("Grade in school"))
				schoolattList.add(attribute);
			else
				attrList.add(attribute);
		}
		ch.setAttributeList(attrList);
		ch.setSchoolAttributeList(schoolattList);
		return ch;
	}
	
	public Children getPersonData(Children ch,Connection con) throws SQLException
	{
		String photoquery = "Select photo_path,age_at_photo from CH_PHOTO where per_id = "+ch.getPerid()+" and is_active = 'Y' and is_main = 'Y'";
		String query = "Select * from PERSON where per_id = "+ch.getPerid();
		String subprjquery = "select sub_id from PROJ_PART where per_id="+ch.getPerid();
		String commquery = "select comm_id,comm_name from COMM where comm_id = (select comm_id from HOUSE where house_id = \r\n" + 
				"(select house_id from PER_UPDATE where\r\n" + 
				"per_id ="+ch.getPerid()+"))";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		String schoolquery = "Select * from School where school_id = "+ch.getSchoolid();
		st = con.createStatement();
		ResultSet rsSchool = st.executeQuery(schoolquery);
		if(rsSchool.next())
		{
			ch.setSchoolName(rsSchool.getString("school_name"));
		}
		
		 while(rs.next())
		 {
			 ch.setChName(rs.getString("per_full"));
			 ch.setDob(new SimpleDateFormat("MM-dd-yyyy").format(rs.getDate("date_of_birth")));
			 ch.setGender(rs.getString("sex_code"));
		 }
		  
		 ch = getAttributePropertyCodeData(ch, con);
		 
		 ResultSet rsphoto = st.executeQuery(photoquery);
		 
		 if(rsphoto.next())
		 {
			 ch.setPhotopath(rsphoto.getString("photo_path"));
			 ch.setAge(rsphoto.getInt("age_at_photo"));
		 }
		 
		 ResultSet rssubprj = st.executeQuery(subprjquery);
		 String subPrj = "";
		 while(rssubprj.next())
		 {
			 subPrj += rssubprj.getInt("sub_id")+",";
		 }
		 ch.setSubPrjlis(subPrj);
		 
		 ResultSet rscomm = st.executeQuery(commquery);
		 if(rscomm.next())
		 {
			 ch.setCommunityname(rscomm.getString("comm_id")+"("+rscomm.getString("comm_name")+")");
		 }
		return ch;
	}

}
