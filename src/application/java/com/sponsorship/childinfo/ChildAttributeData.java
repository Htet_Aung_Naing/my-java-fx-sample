package com.sponsorship.childinfo;

public class ChildAttributeData {
	String attributeCode;
	String propCode;
	public String getAttributeCode() {
		return attributeCode;
	}
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}
	public String getPropCode() {
		return propCode;
	}
	public void setPropCode(String propCode) {
		this.propCode = propCode;
	}
	
	public ChildAttributeData()
	{
		this.attributeCode = "";
		this.propCode = "";
	}

}
