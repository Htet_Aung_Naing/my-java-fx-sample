package com.sponsorship.childinfo;

import java.sql.SQLException;
import java.util.ArrayList;

import com.sponsorship.dao.ChildDao;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class MainUpstream extends Application{

	 SplitPane splitpane = new SplitPane();
	 ScrollPane scroll = new ScrollPane();
	 ScrollPane scrolldetail = new ScrollPane(); 
	 Label lbltcount = new Label();
	 int tcount = 0;
	ListView<Children> lstView = new ListView<>();
	 
	@Override
	public void start(Stage stage) throws Exception 
	{
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainMenu.fxml"));
       
        addUpstreamData(scroll);
        ObservableList<Node> nodeList = root.getChildrenUnmodifiable();
   
        for (Node node : nodeList) {
			if(node.getId().equals("splitPane") )
			{
				splitpane = (SplitPane) node;

				splitpane.getItems().add(0, scroll);

			}
			if(node.getId().equals("lblTcount"))
			{
				lbltcount = (Label) node;
				lbltcount.setText("Total Count: "+String.valueOf(tcount));
			}
		}
        
        Scene scene = new Scene(root);
        stage.getIcons().add(new Image(MainUpstream.class.getResourceAsStream("/image/logo.png")));
        stage.setTitle("Upstream Children List");
        stage.setScene(scene);
        stage.show();
	 }
	 
	    public static void main(String[] args) throws SQLException 
	    {
	        launch(args);
	    	/*ChildDao childdao = new ChildDao();
	    	ArrayList<Children>chList = childdao.getUpstreamChildrenList();
	    	for (Children children : chList) {
				//children.seta
			}
	    	
	    	System.out.println("");*/
	    }
	    
	    public void createChildDetailPane(Children child,ScrollPane scrollpane)
	    {
	    	if(child != null)
	    	{
	    		scrollpane = new ScrollPane();
		    	VBox verticalpanel = new VBox();
		        verticalpanel.setPadding(new Insets(15));
		
		        Image image = new Image("file:///"+child.getPhotopath());
		        ImageView imgView = new ImageView();
		        imgView.setFitWidth(400);
		        imgView.setFitHeight(300);
		        imgView.setPreserveRatio(true);
		        imgView.setImage(image);
		        Label lblcomm = new Label();
		    	Label lblid = new Label();
		    	Label lblchname = new Label();
		    	Label lblgender = new Label();
		    	Label lbldob = new Label();
		    	Label lblSchName = new Label();
		    	Label lblage = new Label();
		    	lblid.setText("ID : "+child.getChId());
		    	lblchname.setText("Name : "+child.getChName());
		    	lblgender.setText("Gender : "+child.getGender());
		    	lbldob.setText("Date of Birth : "+child.getDob());
		    	lblage.setText("Age : "+child.getAge());
		    	lblcomm.setText("Community : "+child.getCommunityname());
		    	lblSchName.setText("School Name : "+child.getSchoolName());
		    	Label subPrjList = new Label();
		    	subPrjList.setText("Sub Project : "+child.getSubPrjlis());
		    	ObservableList list = verticalpanel.getChildren();    
		        list.addAll(imgView,lblid, lblchname, lblgender,lbldob,lblage,subPrjList,lblcomm,lblSchName); 
		         
		        for(ChildAttributeData schoolarribute : child.getSchoolAttributeList())
		        {
		        	Label lbl = new Label();
		    		lbl.setText(schoolarribute.getAttributeCode()+" : "+schoolarribute.getPropCode());
					list.add(lbl);
		        }
		         
		    	for (ChildAttributeData attribute : child.getAttributeList()) {
		    		Label lbl = new Label();
		    		lbl.setText(attribute.getAttributeCode()+" : "+attribute.getPropCode());
					list.add(lbl);
				}
		    	
		    	
		    	scrollpane.setContent(verticalpanel);
		    	splitpane.getItems().set(1,scrollpane);
	    	}
	    	
	    }
	    
	    public void addUpstreamData(ScrollPane scroll) throws SQLException
	    {
	    	ArrayList<Children> childrenList = new ArrayList<Children>();
			ChildDao childDao = new ChildDao();
			childrenList = childDao.getUpstreamChildrenList();
			ObservableList <Children> chList = FXCollections.observableArrayList(childrenList);
			tcount = childrenList.size();
			
		
			lstView.setItems(chList);
			//lstView.setCellFactory(new ListCell());
			
			
			lstView.setCellFactory(new Callback<ListView<Children>, ListCell<Children>>(){

		            public ListCell<Children> call(ListView<Children> p) {
		                
		                ListCell<Children> cell = new ListCell<Children>(){
		               
		                    @Override
		                    protected void updateItem(Children t, boolean bln) {
		                        super.updateItem(t, bln);
		                       
		                        if (t != null) {
		             
									setText(t.getChId()+"("+t.getChName()+")");
		                      
		                        }
		                    }

		                };
		                
		                return cell;
		            }
		        });


			
			lstView.setItems(chList);
			
			lstView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
		
					Children selecteditem = lstView.getSelectionModel().getSelectedItem();
					createChildDetailPane(selecteditem, scrolldetail);
				}
			});
			
			lstView.setOnKeyReleased(new EventHandler<KeyEvent>() {

				@Override
				public void handle(KeyEvent event) {
					if(event.getCode() == KeyCode.UP)
					{
						Children selecteditem = lstView.getSelectionModel().getSelectedItem();
						createChildDetailPane(selecteditem, scrolldetail);
					}else if(event.getCode() == KeyCode.DOWN)
					{
						Children selecteditem = lstView.getSelectionModel().getSelectedItem();
						createChildDetailPane(selecteditem, scrolldetail);
					}
					
				}
			});
			
			lstView.setPrefSize(700, 550);
			scroll.setContent(lstView);
	    }
	}


