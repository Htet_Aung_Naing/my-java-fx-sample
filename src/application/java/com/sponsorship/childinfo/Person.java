package com.sponsorship.childinfo;

public class Person {
	
	String schoolname;
	String child_id;
	int perid;
	String child_name;
	String ineligible_date;
	int school_id;
	
	public int getSchool_id() {
		return school_id;
	}
	public void setSchool_id(int school_id) {
		this.school_id = school_id;
	}
	public int getPerid() {
		return perid;
	}
	public void setPerid(int perid) {
		this.perid = perid;
	}
	public String getSchoolname() {
		return schoolname;
	}
	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}
	public String getChild_id() {
		return child_id;
	}
	public void setChild_id(String child_id) {
		this.child_id = child_id;
	}
	public String getChild_name() {
		return child_name;
	}
	public void setChild_name(String child_name) {
		this.child_name = child_name;
	}
	public String getIneligible_date() {
		return ineligible_date;
	}
	public void setIneligible_date(String ineligible_date) {
		this.ineligible_date = ineligible_date;
	}
	
	public Person()
	{
		this.child_id = "";
		this.child_name = "";
		this.ineligible_date = "";
		this.schoolname = "";
		this.perid = 0;
		this.school_id = 0;
	}

}
