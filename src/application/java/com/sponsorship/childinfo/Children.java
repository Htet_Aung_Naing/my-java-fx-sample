package com.sponsorship.childinfo;

import java.util.ArrayList;

public class Children {
	
	String chName;
	String chId;
	String dob;
	int age;
	String gender;
	String schoolName;
	String photopath;
	String subPrjlis;
	int perid;
	int schoolid;
	String communityname;
	ArrayList<ChildAttributeData> attributeList;
	ArrayList<ChildAttributeData> schoolAttributeList;
	
	public String getCommunityname() {
		return communityname;
	}
	public void setCommunityname(String communityname) {
		this.communityname = communityname;
	}
	public String getSubPrjlis() {
		return subPrjlis;
	}
	public void setSubPrjlis(String subPrjlis) {
		this.subPrjlis = subPrjlis;
	}
	public ArrayList<ChildAttributeData> getSchoolAttributeList() {
		return schoolAttributeList;
	}
	public void setSchoolAttributeList(ArrayList<ChildAttributeData> schoolAttributeList) {
		this.schoolAttributeList = schoolAttributeList;
	}
	public ArrayList<ChildAttributeData> getAttributeList() {
		return attributeList;
	}
	public void setAttributeList(ArrayList<ChildAttributeData> attributeList) {
		this.attributeList = attributeList;
	}
	public Integer getPerid() {
		return perid;
	}
	public void setPerid(int perid) {
		this.perid = perid;
	}

	public String getChName() {
		return chName;
	}
	public void setChName(String chName) {
		this.chName = chName;
	}
	public String getChId() {
		return chId;
	}
	public void setChId(String chId) {
		this.chId = chId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	
	
	public int getSchoolid() {
		return schoolid;
	}
	public void setSchoolid(int schoolid) {
		this.schoolid = schoolid;
	}
	public String getPhotopath() {
		return photopath;
	}
	public void setPhotopath(String photopath) {
		this.photopath = photopath;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public Children()
	{
		this.chId = "";
		this.chName = "";
		this.attributeList = new ArrayList<ChildAttributeData>();
		this.schoolName = "";
		this.dob = "";
		this.gender = "";
		this.perid = 0;
		this.schoolid = 0;
		this.photopath = "";
		this.age = 0;
		this.subPrjlis = "";
		this.schoolAttributeList = new ArrayList<>();
		this.communityname = "";
	}
	

}
