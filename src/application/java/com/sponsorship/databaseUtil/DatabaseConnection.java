package com.sponsorship.databaseUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;



public class DatabaseConnection {
	
	public static Connection getConnection() {
		try {
			 File jarPath=new File(DatabaseConnection.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		        String propertiesPath=jarPath.getParentFile().getAbsolutePath()+"\\dbconfig.properties";
		
			
		        Properties prop = new Properties();
				InputStream input = new FileInputStream(propertiesPath);
				prop.load(input);
				Connection con = null;
				if(prop != null)
				{
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					String connectionurl = "jdbc:sqlserver://scimmrasist:1433;" +  
			         "databaseName="+prop.getProperty("dbname")+";user="+prop.getProperty("username")+";password="+prop.getProperty("password"); 
					con = DriverManager.getConnection(connectionurl);
				}
			
	
			return con;
		} catch (Exception ex) {
			System.out.println("Database.getConnection() Error -->"
					+ ex.getMessage());
			return null;
		}
	}

	public static void close(Connection con) {
		try {
			con.close();
		
		} catch (Exception ex) {
		}
	}
	
	
}

